<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel-vue Crud</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
       
    <body>
        <div id="app">
            <mainapp></mainapp>
        </div>
       
    </body>
    <script src="{{mix('/js/app.js')}}"></script>
    
</html>
