require('./bootstrap');
window.Vue = require('vue');
// import routes 
import router from './router'


import ViewUI from 'view-design';
import locale from 'view-design/dist/locale/en-US';
// import style
import 'view-design/dist/styles/iview.css';
// Vue.use(ViewUI);
Vue.use(ViewUI, { locale });

// import commont mixins
import common from './common'
Vue.mixin(common)
Vue.config.productionTip = false
// initial components for loading vue app
Vue.component('mainapp', require('./components/mainapp.vue').default);
const app = new Vue({
	router,
	el: '#app'
});

