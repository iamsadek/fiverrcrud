import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
import statement from './components/statement.vue'


export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'statement',
      component: statement,
     
    },
   ],

});