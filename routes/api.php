<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/addStatement', 'BankStatementController@addStatement');
Route::get('/getStatement', 'BankStatementController@getStatement'); // get all the item 
Route::get('/getStatemenSingle/{id}', 'BankStatementController@getStatementById'); // get single statement
Route::post('/updateStatement', 'BankStatementController@updateStatement'); 
Route::post('/deleteStatement', 'BankStatementController@deleteStatement'); 