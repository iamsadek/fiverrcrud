<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\MyCompanyScope;
use App\Traits\RecordsActivity;
use App\Traits\Formatter;

class BankStatementItem extends Model
{
    //use SoftDeletes, RecordsActivity, Formatter;

    protected $table = 'bank_statement_items';

    protected $fillable = [
        'bank_statement_id',
        'client_id',
        'konto_id',
        'debits', // potrazuje - plus na iznos
        'credits', // duguje - minus na iznos
        'description'
    ];

    protected $datesFormat = [
    ];

    protected $numbersFormat = [
        'debits',
        'credits',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function bankStatement()
    {
        return $this->belongsTo(BankStatement::class);
    }

}
