<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BankStatementItem;
use Illuminate\Support\Facades\Validator;

class BankStatementController extends Controller
{
    public function addStatement(Request $request){
        //validate the request 
        $validator = Validator::make($request->all(), [
            'bank_statement_id' => 'required|integer',
            'client_id' => 'integer',
            'konto_id' => 'integer',
            'konto_id' => 'integer',
            'debits' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()->all(), 
            ], 401);
           
        }
        $statement = BankStatementItem::create([
            'bank_statement_id' => $request->bank_statement_id,
            'client_id' => $request->client_id,
            'konto_id' => $request->konto_id,
            'debits' => $request->debits,
            'credits' => $request->credits,
            'description' => $request->description,
        ]);
        return response()->json([
            'statement' => $statement, 
            'msg' => 'Bank statement has been created successfully!'
        ]);

        
    
    }
    public function getStatement(){
        return BankStatementItem::all();
    }
    public function getStatementById($id){
        return BankStatementItem::where('id', $id)->first();
    }
    public function updateStatement(Request $request){
        // validate the request so that no empty value is placed.. 
        $validator = Validator::make($request->all(), [
            'bank_statement_id' => 'required|integer',
            'client_id' => 'integer',
            'konto_id' => 'integer',
            'konto_id' => 'integer',
            'debits' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()->all(), 
            ], 401);
        }
        // before processing update make sure the owner is deleting with an extra where condition
        $update = BankStatementItem::where('id', $request->id)->update([
            'bank_statement_id' => $request->bank_statement_id,
            'client_id' => $request->client_id,
            'konto_id' => $request->konto_id,
            'debits' => $request->debits,
            'credits' => $request->credits,
            'description' => $request->description,
        ]);

        if(!$update){
            return response()->json([
                'msg' => 'Something went wrong. Please try again later.'
            ], 401);
        }
        return response()->json([
            'msg' => 'Record has been updated successfully!'
        ]);
    }
    public function deleteStatement(Request $request){
        // before processing delete make sure the owner is deleting with an extra where condition
       $delete =  BankStatementItem::where('id', $request->id)->delete();
       if(!$delete){
            return response()->json([
                'msg' => 'Something went wrong. Please try again later.'
            ], 401);
        }
        return response()->json([
            'msg' => 'Record has been deleted successfully!'
        ]);

    }
}
