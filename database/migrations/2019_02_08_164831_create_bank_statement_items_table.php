<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankStatementItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_statement_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_statement_id');
            $table->integer('client_id')->nullable();
            $table->integer('konto_id')->nullable();
            $table->decimal('debits', 20, 2)->default(0); // potrazuje - plus na iznos
            $table->decimal('credits', 20, 2)->default(0); // duguje - minus na iznos
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_statement_items');
    }
}
